import express from "express";
import rateLimit from "express-rate-limit";

import bunsRouter from "./routes/bunsRouter";
import fillingsRouter from "./routes/fillingsRouter";
import burgersRouter from "./routes/burgersRouter";

const app = express();

app.use(
  rateLimit({
    message: "Way too many requests. Please try again after one minute",
    windowMs: 60 * 1000,
    max: 30,
  })
);

app.use(express.json());

app.use("/api/buns", bunsRouter);
app.use("/api/burgers", burgersRouter);
app.use("/api/fillings", fillingsRouter);

app.listen(3000, () => {
  console.log("App running on port 3000");
});
