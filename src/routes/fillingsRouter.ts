import express from "express";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();
const router = express.Router();

router.get("/", async (req, res) => {
  res.send(await prisma.fillings.findMany({}));
});

router.get("/:id", async (req, res) => {
  const filling = await prisma.fillings.findOne({
    where: {
      id: Number(req.params.id),
    },
  });

  filling ? res.send(filling) : res.sendStatus(404);
});

router.post("/create", async (req, res) => {
  const filling = await prisma.fillings.create({
    data: req.body,
  });

  res.send(filling);
});

export default router;
