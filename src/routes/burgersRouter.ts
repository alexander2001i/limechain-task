import express from "express";
import {
  PrismaClient,
  FindOneBurgerArgs,
  BurgerGetPayload,
} from "@prisma/client";

const prisma = new PrismaClient();
const router = express.Router();

router.get("/", async (req, res) => {
  const burgersPerPage =
    Number(req.query.perPage) > 0 && Number(req.query.perPage) < 25
      ? Number(req.query.perPage)
      : 5;
  const page = Number(req.query.page) > 0 ? Number(req.query.page) - 1 : 0;

  let whereObj: any = {};

  if (req.query.name) whereObj.name = { contains: req.query.name };

  res.send(
    (
      await prisma.burger.findMany({
        include: {
          fillings: true,
          bun: true,
        },
        where: whereObj,
        first: burgersPerPage,
        skip: burgersPerPage * page,
      })
    ).map((burger) => formatBurger(burger))
  );
});

router.get("/random", async (req, res) => {
  const randomId = Math.floor(
    Math.random() * ((await prisma.burger.count()) - 1) + 1
  );

  res.send(
    formatBurger(
      await prisma.burger.findOne({
        where: { id: randomId },
        include: {
          fillings: true,
          bun: true,
        },
      })
    )
  );
});

router.get("/:id", async (req, res) => {
  const burger = await prisma.burger.findOne({
    where: { id: Number(req.params.id) },
    include: {
      fillings: true,
      bun: true,
    },
  });

  burger ? res.send(formatBurger(burger)) : res.sendStatus(404);
});

router.post("/create", async (req, res) => {
  const burgerData = req.body as {
    name: string;
    bunId: number;
    fillingsIds: Array<number>;
  };

  const burger = await prisma.burger.create({
    data: {
      name: burgerData.name,
      bun: {
        connect: {
          id: burgerData.bunId,
        },
      },
      fillings: {
        connect: burgerData.fillingsIds.map((id) => ({ id })),
      },
    },
  });

  res.send(burger);
});

export default router;

// @ts-ignore
const formatBurger = (burger) => {
  return {
    ...burger,
    calories:
      // @ts-ignore
      burger.fillings.reduce((a, b) => a + (b.calories || 0), 0) +
      burger.bun.calories,

    price:
      // @ts-ignore
      burger.fillings.reduce((a, b) => a + (b.price || 0), 0) +
      burger.bun.price,
  };
};
