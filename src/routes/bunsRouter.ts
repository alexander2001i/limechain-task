import express from "express";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();
const router = express.Router();

router.get("/", async (req, res) => {
  res.send(await prisma.bun.findMany({}));
});

router.get("/:id", async (req, res) => {
  res.send(
    await prisma.bun.findOne({
      where: {
        id: Number(req.params.id),
      },
    })
  );
});

router.post("/create", async (req, res) => {
  const bun = await prisma.bun.create({
    data: req.body,
  });

  res.send(bun);
});

export default router;
